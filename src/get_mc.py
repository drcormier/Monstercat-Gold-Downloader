import configparser
import logging
import re
from io import BytesIO
from zipfile import ZipFile

import requests
from tqdm import tqdm

config = configparser.RawConfigParser()
config.read("mc_config.ini")

# url to get all the releases from
releasesurl = "https://connect.monstercat.com/api/catalog/release?page=1" + \
              "&fields=title,renderedArtists,releaseDate,preReleaseDate,coverUrl,catalogId&skip=0&limit=5000"
# url parts to get a download from
# need to put the id in between
downloadurl = "https://connect.monstercat.com/api/release/"
downloadurl2 = "/download?method=download&type=flac"
# log into monstercat online and use the developer console to get connect.sid
headers = {'cookie': "connect.sid={}".format(config['USERINFO']['connect'])}

logging.basicConfig(filename="errors.log", level=logging.ERROR)


def download(id, title):
	"""
	downloads a given monstercat release by id
	:param id: the id of the release to download
	:param title: the title of the release
	:return: None
	"""
	try:
		r = requests.get(downloadurl + id + downloadurl2, headers=headers, stream=True)
		# grab data from headers
		total_length = int(r.headers.get('content-length'))
		size = total_length / (1024 * 1024)
		d = r.headers['content-disposition']
		fname = re.findall("filename=(.+)", d)
		# strip invalid characters
		path = fname.pop(0).translate({ord(c): None for c in u'\":\\/|'})
		print("Downloading {} ({:.2f}MiB)".format(title, size, ))
		chunk_size = 1024 * 1024
		bars = int(total_length / chunk_size)
		# double copies to disk but keeps zip if it fails to unzip
		with BytesIO() as f:
			for chunk in tqdm(r.iter_content(chunk_size=chunk_size), total=bars, unit='MB', ):
				if chunk:
					f.write(chunk)
			# send data to unzipper
			zf = ZipFile(f)
			zf.extractall(path=title.translate({ord(c): None for c in u'\":\\/|'}))
			with open("downloadedids.txt", 'a') as g:
				g.write('|' + id)
	except requests.exceptions.RequestException as e:
		logging.error(e)
		return


def main():
	print("Getting releases...")
	releasesReq = requests.get(releasesurl, headers=headers)

	releases = releasesReq.json()
	if releasesReq.status_code is not 200:
		print('ERROR: request was unsuccessful: {} {}'.format(releasesReq.status_code, releasesReq.reason))

	releaseLookup = {}
	# map id to title
	for release in releases[u'results']:
		if release[u'downloadable'] is True:
			releaseLookup[release[u'_id']] = release[u'title']

	# skip downloaded games
	with open("downloadedids.txt", 'r') as f:
		downloaded = []
		for i in f.read().split('|'):
			downloaded.append(i)
	print('{} releases to download'.format(len([item for item in releaseLookup.keys() if item not in downloaded])))
	for k, v in releaseLookup.items():
		if k not in downloaded:
			download(k, v)
	print("Done!")


if __name__ == '__main__':
	main()
